object Problems extends App {
//  LIST SOLUTION FUNCTIONS

// P01 (*) Find the last element of a list.
  def last(theList: List[Int]) {
    val listLength = theList.length;
    try {
      println("The last element is: " + theList(listLength - 1))
    } catch {
      case e: IndexOutOfBoundsException => println(e)
    }

  }

// P02 (*) Find the last but one element of a list.
  def penultimate(theList: List[Int]) {
    val listLength = theList.length;
    try {
      println("The last but one element is: " + theList(listLength - 2))
    } catch {
      case e: IndexOutOfBoundsException => println(e)
    }
  }

// P03 (*) Find the Kth element of a list.
  def nth(index: Int, theList: List[Int]) {
    try {
      println("The " + index + " element is: " + theList(index))
    } catch {
      case e: IndexOutOfBoundsException => println(e)
    }
  }

// P04 (*) Find the number of elements of a list.
  def length(theList: List[Int]) {
    println("The list has: " + theList.length + " elements")
  }

// P05 (*) Reverse a list.
  def reverse(theList: List[Int]) {
    println("The reversed list is: " + theList.reverse)
  }

//   P06 (*) Find out whether a list is a palindrome.
  def isPalindrome(theList: List[Int]) {
    val reversedList = theList.reverse
    if (theList == reversedList) {
      println("Is palindrome? " + true)
    } else {
      println("Is palindrome? " + false)
    }
  }

//   P07 (**) Flatten a nested list structure.
// def flatten (theList: List[Any]){
//     val flattenedList = theList.flatten
//     println("The flattened list is: "+ flattenedList)
// }

def flatten(theList:List[Any]):List[Any] = theList flatMap {
case i: List[_] => flatten(i)
case e => List(e)
}

  val myList = List(1, 1, 2, 3, 5, 8)
  last(myList)
  penultimate(myList)
  nth(4, myList)
  length(myList)
  reverse(myList)
  isPalindrome(List(1, 2, 3, 2, 1))
  print(flatten(List(List(1, 1), 2, List(3, List(5, 8)))))

}
